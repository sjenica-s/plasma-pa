# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Yoann Laissus <yoann.laissus@gmail.com>, 2015, 2016.
# Vincent Pinon <vpinon@kde.org>, 2016, 2017.
# Johan Claude-Breuninger <johan.claudebreuninger@gmail.com>, 2017.
# Simon Depiets <sdepiets@gmail.com>, 2018, 2019, 2020.
# Xavier Besnard <xavier.besnard@neuf.fr>, 2021, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-11-03 00:48+0000\n"
"PO-Revision-Date: 2022-10-18 07:20+0200\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 22.08.2\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: contents/ui/DeviceListItem.qml:26
#, kde-format
msgctxt "label of device items"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: contents/ui/DeviceListItem.qml:35
#, kde-format
msgid "Device name not found"
msgstr "Nom du périphérique introuvable"

#: contents/ui/ListItemBase.qml:70
#, kde-format
msgid "Currently not recording"
msgstr "Actuellement, hors enregistrement"

#: contents/ui/ListItemBase.qml:71
#, kde-format
msgid "Currently not playing"
msgstr "Actuellement, hors lecture"

#: contents/ui/ListItemBase.qml:184
#, kde-format
msgctxt "@action:button"
msgid "Additional Options"
msgstr "Option supplémentaires"

#: contents/ui/ListItemBase.qml:186
#, kde-format
msgid "Show additional options for %1"
msgstr "Afficher les option supplémentaires pour %1"

#: contents/ui/ListItemBase.qml:203
#, kde-format
msgctxt "@action:button"
msgid "Unmute"
msgstr "Restaurer le son"

#: contents/ui/ListItemBase.qml:203
#, kde-format
msgctxt "@action:button"
msgid "Mute"
msgstr "Couper le son"

#: contents/ui/ListItemBase.qml:205
#, kde-format
msgid "Unmute %1"
msgstr "Remettre le son pour %1"

#: contents/ui/ListItemBase.qml:205
#, kde-format
msgid "Mute %1"
msgstr "Couper le son de %1"

#: contents/ui/ListItemBase.qml:225
#, kde-format
msgctxt "Accessibility data on volume slider"
msgid "Adjust volume for %1"
msgstr "Ajuster le volume de %1"

#: contents/ui/ListItemBase.qml:283
#, kde-format
msgctxt "volume percentage"
msgid "%1%"
msgstr "%1%"

#: contents/ui/ListItemBase.qml:303
#, kde-format
msgctxt "only used for sizing, should be widest possible string"
msgid "100%"
msgstr "100 %"

#: contents/ui/main.qml:32
#, kde-format
msgid "Audio Volume"
msgstr "Volume audio"

#: contents/ui/main.qml:56
#, kde-format
msgid "Audio Muted"
msgstr "Son coupé"

#: contents/ui/main.qml:58
#, kde-format
msgid "Volume at %1%"
msgstr "Volume à %1 %"

#: contents/ui/main.qml:75
#, kde-format
msgid "Middle-click to unmute"
msgstr "Faites un clic central pour restaurer le son"

#: contents/ui/main.qml:76
#, kde-format
msgid "Middle-click to mute all audio"
msgstr "Faites un clic central pour couper tous les flux audio"

#: contents/ui/main.qml:237
#, kde-format
msgid "No output device"
msgstr "Aucun périphérique de sortie"

#: contents/ui/main.qml:369
#, kde-format
msgid "Increase Volume"
msgstr "Augmenter le volume"

#: contents/ui/main.qml:375
#, kde-format
msgid "Decrease Volume"
msgstr "Diminuer le volume"

#: contents/ui/main.qml:381
#, kde-format
msgid "Mute"
msgstr "Couper le son"

#: contents/ui/main.qml:387
#, kde-format
msgid "Increase Microphone Volume"
msgstr "Augmenter le volume du microphone"

#: contents/ui/main.qml:393
#, kde-format
msgid "Decrease Microphone Volume"
msgstr "Diminuer le volume du microphone"

#: contents/ui/main.qml:399
#, kde-format
msgid "Mute Microphone"
msgstr "Couper le son du microphone"

#: contents/ui/main.qml:503
#, kde-format
msgid "Devices"
msgstr "Périphériques"

#: contents/ui/main.qml:510
#, kde-format
msgid "Applications"
msgstr "Applications"

#: contents/ui/main.qml:531 contents/ui/main.qml:533 contents/ui/main.qml:759
#, kde-format
msgid "Force mute all playback devices"
msgstr "Couper le son de tous les périphériques de lecture"

#: contents/ui/main.qml:567
#, kde-format
msgid "No output or input devices found"
msgstr "Aucun périphérique d'entrée ou de sortie trouvé"

#: contents/ui/main.qml:586
#, kde-format
msgid "No applications playing or recording audio"
msgstr ""
"Aucune application ne lit ou n'enregistre actuellement du contenu sonore"

#: contents/ui/main.qml:737
#, kde-format
msgid "Raise maximum volume"
msgstr "Augmenter le volume maximum"

#: contents/ui/main.qml:763
#, kde-format
msgid "Show virtual devices"
msgstr "Afficher les périphériques virtuels"

#: contents/ui/main.qml:769
#, kde-format
msgid "&Configure Audio Devices…"
msgstr "&Configurer les périphériques audio..."

#: contents/ui/StreamListItem.qml:24
#, kde-format
msgid "Stream name not found"
msgstr "Nom du flux introuvable"

#~ msgid "General"
#~ msgstr "Général"

#~ msgid "Volume step:"
#~ msgstr "Palier de volume :"

#~ msgid "Play audio feedback for changes to:"
#~ msgstr "Jouer un retour audio lors des changements de :"

#~ msgid "Audio volume"
#~ msgstr "Volume audio"

#~ msgid "Show visual feedback for changes to:"
#~ msgstr "Afficher un retour visuel pour les changements de :"

#~ msgid "Microphone sensitivity"
#~ msgstr "Sensibilité du microphone"

#~ msgid "Mute state"
#~ msgstr "Muet"

#~ msgid "Default output device"
#~ msgstr "Périphérique de sortie par défaut"

#~ msgctxt "@title"
#~ msgid "Display:"
#~ msgstr "Affichage :"

#~ msgid "Record all audio via this device"
#~ msgstr "Enregistrer tous les sons via ce périphérique"

#~ msgid "Play all audio via this device"
#~ msgstr "Jouer tous les sons via ce périphérique"

#~ msgctxt ""
#~ "Heading for a list of ports of a device (for example built-in laptop "
#~ "speakers or a plug for headphones)"
#~ msgid "Ports"
#~ msgstr "Ports"

#~ msgctxt "Port is unavailable"
#~ msgid "%1 (unavailable)"
#~ msgstr "%1 (indisponible)"

#~ msgctxt "Port is unplugged"
#~ msgid "%1 (unplugged)"
#~ msgstr "%1 (débranché)"

#~ msgctxt ""
#~ "Heading for a list of possible output devices (speakers, headphones, ...) "
#~ "to choose"
#~ msgid "Play audio using"
#~ msgstr "Jouer le son en utilisant"

#~ msgctxt ""
#~ "Heading for a list of possible input devices (built-in microphone, "
#~ "headset, ...) to choose"
#~ msgid "Record audio using"
#~ msgstr "Enregistrer le son en utilisant"

#~ msgid "show hidden devices"
#~ msgstr "afficher les périphériques cachés"

#~ msgid "Feedback:"
#~ msgstr "Retour :"

#~ msgid "Play sound when volume changes"
#~ msgstr "Jouer un son lorsque le volume est modifié"

#~ msgid "Display notification when default output device changes"
#~ msgstr ""
#~ "Afficher une notification lors des changements de périphérique de sortie "
#~ "par défaut"

#~ msgid "Maximum volume:"
#~ msgstr "Volume maximum :"

#~ msgid "Default Device"
#~ msgstr "Périphérique par défaut"

#~ msgid "Playback Streams"
#~ msgstr "Flux de lecture"

#~ msgid "Recording Streams"
#~ msgstr "Flux d'enregistrement"

#~ msgid "Playback Devices"
#~ msgstr "Périphériques de lecture"

#~ msgid "Recording Devices"
#~ msgstr "Périphériques d'enregistrement"

#~ msgctxt "label of stream items"
#~ msgid "%1 (%2)"
#~ msgstr "%1 (%2)"

#~ msgctxt "Checkable switch for (un-)muting sound output."
#~ msgid "Mute"
#~ msgstr "Muet"

#~ msgctxt "Checkable switch to change the current default output."
#~ msgid "Default"
#~ msgstr "Par défaut"

#~ msgid "Capture Streams"
#~ msgstr "Flux de capture"

#~ msgid "Capture Devices"
#~ msgstr "Périphériques de capture"

#~ msgid "Volume"
#~ msgstr "Volume"

#~ msgid "%"
#~ msgstr "%"

#~ msgid "Behavior"
#~ msgstr "Comportement"

#~ msgid "Volume feedback"
#~ msgstr "Émettre un son lorsque le volume est modifié"

#~ msgid "Device Profiles"
#~ msgstr "Profils de périphériques"

#~ msgctxt "@label"
#~ msgid "No Device Profiles Available"
#~ msgstr "Aucun profil de périphérique disponible"

#~ msgid "Advanced Output Configuration"
#~ msgstr "Configuration avancée des sorties"

#~ msgid ""
#~ "Add virtual output device for simultaneous output on all local sound cards"
#~ msgstr ""
#~ "Ajouter un périphérique de lecture virtuel pour lire simultanément sur "
#~ "toutes les cartes sons"

#~ msgid ""
#~ "Automatically switch all running streams when a new output becomes "
#~ "available"
#~ msgstr ""
#~ "Commuter automatiquement tous les flux en cours quand une nouvelle sortie "
#~ "devient disponible"

#~ msgid "Requires 'module-gconf' PulseAudio module"
#~ msgstr "Nécessite le module PulseAudio « module-gconf »"

#~ msgid "Speaker Placement and Testing"
#~ msgstr "Emplacement et test du haut-parleur"

#~ msgctxt "@label"
#~ msgid "Output:"
#~ msgstr "Sortie :"

#~ msgid "Front Left"
#~ msgstr "Avant gauche"

#~ msgid "Front Center"
#~ msgstr "Avant centre"

#~ msgid "Front Right"
#~ msgstr "Avant droit"

#~ msgid "Side Left"
#~ msgstr "Latéral gauche"

#~ msgid "Side Right"
#~ msgstr "Latéral droit"

#~ msgid "Rear Left"
#~ msgstr "Arrière gauche"

#~ msgid "Subwoofer"
#~ msgstr "Caisson de basses"

#~ msgid "Rear Right"
#~ msgstr "Arrière droit"

#~ msgid "Playback"
#~ msgstr "Lecture"

#~ msgctxt "@label"
#~ msgid "No Applications Playing Audio"
#~ msgstr "Aucune application ne lit actuellement du contenu sonore"

#~ msgid "Capture"
#~ msgstr "Capturer"

#~ msgctxt "@label"
#~ msgid "No Applications Recording Audio"
#~ msgstr "Aucune application n'enregistre actuellement du contenu sonore"

#~ msgctxt "@label"
#~ msgid "Profile:"
#~ msgstr "Profil :"

#~ msgid "Port"
#~ msgstr "Port"

#~ msgid "Outputs"
#~ msgstr "Sorties"

#~ msgctxt "@label"
#~ msgid "No Output Devices Available"
#~ msgstr "Aucun périphérique de sortie disponible"

#~ msgid "Inputs"
#~ msgstr "Entrées"

#~ msgctxt "@label"
#~ msgid "No Input Devices Available"
#~ msgstr "Aucun périphérique d'entrée disponible"

#~ msgid "This module allows configuring the Pulseaudio sound subsystem."
#~ msgstr "Ce module permet de configurer le sous-système sonore PulseAudio"

#~ msgctxt "@title:tab"
#~ msgid "Devices"
#~ msgstr "Périphériques d"

#~ msgctxt "@title:tab"
#~ msgid "Applications"
#~ msgstr "Applications"

#~ msgctxt "@title:tab"
#~ msgid "Advanced"
#~ msgstr "Avancé"

#~ msgid "Notification Sounds"
#~ msgstr "Sons de notification"

#~ msgctxt "label of stream items"
#~ msgid "%1: %2"
#~ msgstr "%1: %2"

#~ msgid "100%"
#~ msgstr "100 %"
