# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# A S Alam <aalam@users.sf.net>, 2016, 2019, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-11-04 00:47+0000\n"
"PO-Revision-Date: 2021-10-06 18:36-0700\n"
"Last-Translator: A S Alam <aalam@satluj.org>\n"
"Language-Team: Punjabi <punjabi-users@lists.sf.net>\n"
"Language: pa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.04.3\n"

#: context.cpp:596
#, kde-format
msgctxt "Name shown in debug pulseaudio tools"
msgid "Plasma PA"
msgstr "ਪਲਾਜ਼ਮਾ ਪੀਏ"

#: kcm/package/contents/ui/CardListItem.qml:52
#: kcm/package/contents/ui/DeviceListItem.qml:127
#, kde-format
msgctxt "@label"
msgid "Profile:"
msgstr "ਪਰੋਫਾਈਲ:"

#: kcm/package/contents/ui/DeviceListItem.qml:47
#, kde-format
msgctxt "label of device items"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: kcm/package/contents/ui/DeviceListItem.qml:88
#, kde-format
msgid "Port:"
msgstr "ਪੋਰਟ:"

#: kcm/package/contents/ui/DeviceListItem.qml:111 qml/listitemmenu.cpp:387
#, kde-format
msgctxt "Port is unavailable"
msgid "%1 (unavailable)"
msgstr "%1 (ਨਾ ਉਪਲੱਬਧ)"

#: kcm/package/contents/ui/DeviceListItem.qml:113 qml/listitemmenu.cpp:389
#, kde-format
msgctxt "Port is unplugged"
msgid "%1 (unplugged)"
msgstr "%1 (ਪਲੱਗ ਕੱਢੇ)"

#: kcm/package/contents/ui/DeviceListItem.qml:180
#, kde-format
msgctxt "Placeholder is channel name"
msgid "%1:"
msgstr "%1:"

#: kcm/package/contents/ui/DeviceListItem.qml:213
#, kde-format
msgctxt "Perform an audio test of the device"
msgid "Test"
msgstr "ਟੈਸਟ"

#: kcm/package/contents/ui/DeviceListItem.qml:222
#, kde-format
msgctxt "Audio balance (e.g. control left/right volume individually"
msgid "Balance"
msgstr "ਬੈਲਨਸ"

#: kcm/package/contents/ui/main.qml:24
#, kde-format
msgid "This module allows configuring the Pulseaudio sound subsystem."
msgstr "ਇਹ ਮੋਡੀਊਲ ਪਲੱਸਆਡੀਓ ਸਾਊਂਡ ਸਬ-ਸਿਸਟਮ ਦੀ ਸੰਰਚਨਾ ਲਈ ਸਹਾਇਕ ਹੈ।"

#: kcm/package/contents/ui/main.qml:105
#, kde-format
msgid "Playback Devices"
msgstr "ਪਲੇਅਬੈਕ ਡਿਵਾਈਸ"

#: kcm/package/contents/ui/main.qml:129
#, kde-format
msgid "Recording Devices"
msgstr "ਰਿਕਾਰਡਿੰਗ ਡਿਵਾਈਸ"

#: kcm/package/contents/ui/main.qml:153
#, kde-format
msgid "Inactive Cards"
msgstr "ਨਾ-ਸਰਗਰਮ ਕਾਰਡ"

#: kcm/package/contents/ui/main.qml:187
#, kde-format
msgid "Playback Streams"
msgstr "ਪਲੇਅਬੈਕ ਸਟਰੀਮਾਂ"

#: kcm/package/contents/ui/main.qml:236
#, kde-format
msgid "Recording Streams"
msgstr "ਰਿਕਾਰਡਿੰਗ ਸਟਰੀਮਾਂ"

#: kcm/package/contents/ui/main.qml:271
#, kde-format
msgid "Show Inactive Devices"
msgstr "ਨਾ-ਸਰਗਰਮ ਡਿਵਾਈਸ ਵੇਖਾਓ"

#: kcm/package/contents/ui/main.qml:279
#, fuzzy, kde-format
#| msgid "Configure…"
msgid "Configure Volume Controls…"
msgstr "…ਸੰਰਚਨਾ"

#: kcm/package/contents/ui/main.qml:288
#, kde-format
msgid "Configure…"
msgstr "…ਸੰਰਚਨਾ"

#: kcm/package/contents/ui/main.qml:292
#, kde-format
msgid "Requires %1 PulseAudio module"
msgstr "%1 ਪਲਸਆਡੀਓ ਮੋਡੀਊਲ ਚਾਹੀਦਾ ਹੈ"

#: kcm/package/contents/ui/main.qml:304
#, kde-format
msgid ""
"Add virtual output device for simultaneous output on all local sound cards"
msgstr ""

#: kcm/package/contents/ui/main.qml:310
#, kde-format
msgid ""
"Automatically switch all running streams when a new output becomes available"
msgstr ""

#: kcm/package/contents/ui/main.qml:326
#, kde-format
msgctxt ""
"%1 is an error string produced by an external component, and probably "
"untranslated"
msgid ""
"Error trying to play a test sound. \n"
"The system said: \"%1\""
msgstr ""

#: kcm/package/contents/ui/main.qml:361
#, kde-format
msgid "Front Left"
msgstr "ਅੱਗੇ ਖੱਬਾ"

#: kcm/package/contents/ui/main.qml:362
#, kde-format
msgid "Front Center"
msgstr "ਅੱਗੇ ਸੈਂਟਰ"

#: kcm/package/contents/ui/main.qml:363
#, kde-format
msgid "Front Right"
msgstr "ਅੱਗੇ ਸੱਜਾ"

#: kcm/package/contents/ui/main.qml:364
#, kde-format
msgid "Side Left"
msgstr "ਪਾਸੇ ਖੱਬਾ"

#: kcm/package/contents/ui/main.qml:365
#, kde-format
msgid "Side Right"
msgstr "ਪਾਸੇ ਸੱਜਾ"

#: kcm/package/contents/ui/main.qml:366
#, kde-format
msgid "Rear Left"
msgstr "ਪਿੱਛੇ ਖੱਬਾ"

#: kcm/package/contents/ui/main.qml:367
#, kde-format
msgid "Subwoofer"
msgstr "ਸਬ-ਵੂਫ਼ਰ"

#: kcm/package/contents/ui/main.qml:368
#, kde-format
msgid "Rear Right"
msgstr "ਪਿੱਛੇ ਸੱਜਾ"

#: kcm/package/contents/ui/main.qml:369
#, kde-format
msgid "Mono"
msgstr "ਮੋਨੋ"

#: kcm/package/contents/ui/main.qml:479
#, kde-format
msgid "Click on any speaker to test sound"
msgstr ""

#: kcm/package/contents/ui/MuteButton.qml:22
#, fuzzy, kde-format
#| msgctxt "Mute audio stream"
#| msgid "Mute %1"
msgctxt "Unmute audio stream"
msgid "Unmute %1"
msgstr "%1 ਚੁੱਪ"

#: kcm/package/contents/ui/MuteButton.qml:22
#, kde-format
msgctxt "Mute audio stream"
msgid "Mute %1"
msgstr "%1 ਚੁੱਪ"

#: kcm/package/contents/ui/StreamListItem.qml:57
#, kde-format
msgid "Notification Sounds"
msgstr "ਨੋਟੀਫਿਕੇਸ਼ਨ ਸਾਊਂਡ"

#: kcm/package/contents/ui/StreamListItem.qml:63
#, kde-format
msgctxt "label of stream items"
msgid "%1: %2"
msgstr "%1: %2"

#: kcm/package/contents/ui/VolumeControlsConfig.qml:15
#, kde-format
msgid "Volume Controls"
msgstr ""

#: kcm/package/contents/ui/VolumeControlsConfig.qml:31
#, kde-format
msgid "Raise maximum volume"
msgstr ""

#: kcm/package/contents/ui/VolumeControlsConfig.qml:39
#, kde-format
msgid "Volume change step:"
msgstr ""

#: kcm/package/contents/ui/VolumeControlsConfig.qml:63
#, kde-format
msgid "Play audio feedback for changes to:"
msgstr ""

#: kcm/package/contents/ui/VolumeControlsConfig.qml:64
#: kcm/package/contents/ui/VolumeControlsConfig.qml:73
#, kde-format
msgid "Audio volume"
msgstr ""

#: kcm/package/contents/ui/VolumeControlsConfig.qml:72
#, kde-format
msgid "Show visual feedback for changes to:"
msgstr ""

#: kcm/package/contents/ui/VolumeControlsConfig.qml:80
#, fuzzy, kde-format
#| msgid "Microphone Muted"
msgid "Microphone sensitivity"
msgstr "ਮਾਈਕਰੋਫ਼ੋਨ ਮੌਨ ਕੀਤਾ"

#: kcm/package/contents/ui/VolumeControlsConfig.qml:87
#, fuzzy, kde-format
#| msgid "Mute audio"
msgid "Mute state"
msgstr "ਆਡੀਓ ਨੂੰ ਮੌਨ ਕਰੋ"

#: kcm/package/contents/ui/VolumeControlsConfig.qml:94
#, fuzzy, kde-format
#| msgid "Default Device"
msgid "Default output device"
msgstr "ਮੂਲ ਡਿਵਾਈਸ"

#: kcm/package/contents/ui/VolumeSlider.qml:51
#, kde-format
msgctxt "volume percentage"
msgid "%1%"
msgstr "%1%"

#: kcm/package/contents/ui/VolumeSlider.qml:71
#, kde-format
msgctxt "only used for sizing, should be widest possible string"
msgid "100%"
msgstr "100%"

#: qml/listitemmenu.cpp:342
#, kde-format
msgid "Play all audio via this device"
msgstr "ਇਸ ਡਿਵਾਈਸ ਰਾਹੀਂ ਸਭ ਆਡੀਓ ਚਲਾਓ"

#: qml/listitemmenu.cpp:347
#, kde-format
msgid "Record all audio via this device"
msgstr "ਇਸ ਡਿਵਾਈਸ ਰਾਹੀਂ ਸਭ ਆਡੀਓ ਰਿਕਾਰਡ ਕਰੋ"

#: qml/listitemmenu.cpp:375
#, kde-format
msgctxt ""
"Heading for a list of ports of a device (for example built-in laptop "
"speakers or a plug for headphones)"
msgid "Ports"
msgstr "ਪੋਰਟ"

#: qml/listitemmenu.cpp:445
#, kde-format
msgctxt ""
"Heading for a list of device profiles (5.1 surround sound, stereo, speakers "
"only, ...)"
msgid "Profiles"
msgstr "ਪਰੋਫਾਈਲ"

#: qml/listitemmenu.cpp:479
#, kde-format
msgctxt ""
"Heading for a list of possible output devices (speakers, headphones, ...) to "
"choose"
msgid "Play audio using"
msgstr "ਇਹ ਵਰਤ ਕੇ ਆਡੀਓ ਚਲਾਓ"

#: qml/listitemmenu.cpp:481
#, kde-format
msgctxt ""
"Heading for a list of possible input devices (built-in microphone, "
"headset, ...) to choose"
msgid "Record audio using"
msgstr "ਇਹ ਵਰਤ ਕੇ ਆਡੀਓ ਰਿਕਾਰਡ ਕਰੋ"

#: qml/microphoneindicator.cpp:102
#, kde-format
msgid "Mute"
msgstr "ਚੁੱਪ"

#: qml/microphoneindicator.cpp:132 qml/microphoneindicator.cpp:134
#, kde-format
msgid "Microphone"
msgstr "ਮਾਈਕਰੋਫ਼ੋਨ"

#: qml/microphoneindicator.cpp:134
#, kde-format
msgid "Microphone Muted"
msgstr "ਮਾਈਕਰੋਫ਼ੋਨ ਮੌਨ ਕੀਤਾ"

#: qml/microphoneindicator.cpp:284
#, kde-format
msgctxt "list separator"
msgid ", "
msgstr ", "

#: qml/microphoneindicator.cpp:284
#, kde-format
msgctxt "List of apps is using mic"
msgid "%1 are using the microphone"
msgstr "%1 ਮਾਈਕਰੋਫ਼ੋਨ ਵਰਤ ਰਹੀ ਹੈ"

#: qml/microphoneindicator.cpp:310
#, kde-format
msgctxt "App %1 is using mic with name %2"
msgid "%1 is using the microphone (%2)"
msgstr "%1 ਮਾਈਕਰੋਫ਼ੋਨ (%2) ਵਰਤ ਰਹੀ ਹੈ"

#: qml/microphoneindicator.cpp:317
#, kde-format
msgctxt "App is using mic"
msgid "%1 is using the microphone"
msgstr "%1 ਮਾਈਕਰੋਫ਼ੋਨ ਵਰਤ ਰਹੀ ਹੈ"

#~ msgid "100%"
#~ msgstr "100%"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "ਅਮਨਪਰੀਤ ਸਿੰਘ ਆਲਮ"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "alam.yellow@gmail.com"

#~ msgctxt "@title"
#~ msgid "Audio"
#~ msgstr "ਆਡੀਓ"

#~ msgctxt "@info:credit"
#~ msgid "Copyright 2015 Harald Sitter"
#~ msgstr "Copyright 2015 Harald Sitter"

#~ msgctxt "@info:credit"
#~ msgid "Harald Sitter"
#~ msgstr "ਹਾਰਾਲਡ ਸਿੱਟਰ"

#~ msgctxt "@info:credit"
#~ msgid "Author"
#~ msgstr "ਲੇਖਕ"

#~ msgid "Configure"
#~ msgstr "ਸੰਰਚਨਾ"

#~ msgid "Device Profiles"
#~ msgstr "ਡਿਵਾਈਸ ਪਰੋਫਾਈਲ"

#~ msgid "Advanced Output Configuration"
#~ msgstr "ਤਕਨੀਕੀ ਆਉਟਪੁੱਟ ਸੰਰਚਨਾ"

#~ msgid "Speaker Placement and Testing"
#~ msgstr "ਸਪੀਕਰ ਟਿਕਾਣਾ ਅਤੇ ਟੈਸਟਿੰਗ"

#~ msgctxt "@label"
#~ msgid "Output:"
#~ msgstr "ਆਉਟਪੁੱਟ:"

#~ msgctxt "Port is unavailable"
#~ msgid " (unavailable)"
#~ msgstr "(ਨਾ ਉਪਲੱਬਧ)"

#~ msgctxt "Port is unplugged"
#~ msgid " (unplugged)"
#~ msgstr "(ਪਲੱਗ ਕੱਢੇ)"

#~ msgid "Configure..."
#~ msgstr "...ਸੰਰਚਨਾ"

#~ msgctxt "@label"
#~ msgid "No Device Profiles Available"
#~ msgstr "ਕੋਈ ਡਿਵਾਈਸ ਪਰੋਫਾਈਲ ਮੌਜੂਦ ਨਹੀਂ ਹੈ"

#~ msgctxt "@label"
#~ msgid "No Playback Devices Available"
#~ msgstr "ਕੋਈ ਪਲੇਅਬੈਕ ਡਿਵਾਈਸ ਉਪਲਬਧ ਨਹੀਂ ਹੈ"

#~ msgctxt "@label"
#~ msgid "No Recording Devices Available"
#~ msgstr "ਕੋਈ ਰਿਕਾਰਡਿੰਗ ਡਿਵਾਈਸ ਉਪਲਬਧ ਨਹੀਂ"

#~ msgctxt "@label"
#~ msgid "No Applications Playing Audio"
#~ msgstr "ਕੋਈ ਐਪਲੀਕੇਸ਼ਨ ਆਡੀਓ ਨੂੰ ਨਹੀਂ ਚਲਾ ਰਹੀ ਹੈ"

#~ msgctxt "@label"
#~ msgid "No Applications Recording Audio"
#~ msgstr "ਕੋਈ ਐਪਲੀਕੇਸ਼ਨ ਆਡੀਓ ਨੂੰ ਰਿਕਾਰਡ ਨਹੀਂ ਕਰ ਰਹੀ ਹੈ"

#~ msgctxt "@title:tab"
#~ msgid "Devices"
#~ msgstr "ਡਿਵਾਈਸ"

#~ msgctxt "@title:tab"
#~ msgid "Applications"
#~ msgstr "ਐਪਲੀਕੇਸ਼ਨਾਂ"

#~ msgctxt "@title:tab"
#~ msgid "Advanced"
#~ msgstr "ਤਕਨੀਕੀ"

#, fuzzy
#~| msgctxt "@title:tab"
#~| msgid "Output Devices"
#~ msgid "Outputs"
#~ msgstr "ਆਉਟਪੁੱਟ ਜੰਤਰ"

#~ msgctxt "@label"
#~ msgid "No Output Devices Available"
#~ msgstr "ਕੋਈ ਆਉਟਪੁੱਟ ਜੰਤਰ (ਡਿਵਾਈਸ) ਮੌਜੂਦ ਨਹੀਂ ਹੈ"

#, fuzzy
#~| msgctxt "@title:tab"
#~| msgid "Input Devices"
#~ msgid "Inputs"
#~ msgstr "ਇੰਪੁੱਟ ਜੰਤਰ"

#~ msgctxt "@label"
#~ msgid "No Additional Configuration Available"
#~ msgstr "ਕੋਈ ਹੋਰ ਸੰਰਚਨਾ ਮੌਜੂਦ ਨਹੀਂ ਹੈ"
