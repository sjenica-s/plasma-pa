# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Jeff Huang <s8321414@gmail.com>, 2016, 2017.
# pan93412 <pan93412@gmail.com>, 2018, 2019, 2020.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_org.kde.plasma.volume\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-11-03 00:48+0000\n"
"PO-Revision-Date: 2020-04-12 00:42+0800\n"
"Last-Translator: Yi-Jyun Pan <pan93412@gmail.com>\n"
"Language-Team: Chinese <zh-l10n@lists.linux.org.tw>\n"
"Language: Traditional Chinese\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 19.12.3\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: contents/ui/DeviceListItem.qml:26
#, kde-format
msgctxt "label of device items"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: contents/ui/DeviceListItem.qml:35
#, kde-format
msgid "Device name not found"
msgstr "找不到裝置名稱"

#: contents/ui/ListItemBase.qml:70
#, kde-format
msgid "Currently not recording"
msgstr ""

#: contents/ui/ListItemBase.qml:71
#, kde-format
msgid "Currently not playing"
msgstr ""

#: contents/ui/ListItemBase.qml:184
#, fuzzy, kde-format
#| msgid "Show additional options for %1"
msgctxt "@action:button"
msgid "Additional Options"
msgstr "顯示 %1 的額外選項"

#: contents/ui/ListItemBase.qml:186
#, kde-format
msgid "Show additional options for %1"
msgstr "顯示 %1 的額外選項"

#: contents/ui/ListItemBase.qml:203
#, fuzzy, kde-format
#| msgid "Mute %1"
msgctxt "@action:button"
msgid "Unmute"
msgstr "靜音 %1"

#: contents/ui/ListItemBase.qml:203
#, fuzzy, kde-format
#| msgid "Mute"
msgctxt "@action:button"
msgid "Mute"
msgstr "靜音"

#: contents/ui/ListItemBase.qml:205
#, fuzzy, kde-format
#| msgid "Mute %1"
msgid "Unmute %1"
msgstr "靜音 %1"

#: contents/ui/ListItemBase.qml:205
#, kde-format
msgid "Mute %1"
msgstr "靜音 %1"

#: contents/ui/ListItemBase.qml:225
#, kde-format
msgctxt "Accessibility data on volume slider"
msgid "Adjust volume for %1"
msgstr "調整 %1 的音量"

#: contents/ui/ListItemBase.qml:283
#, kde-format
msgctxt "volume percentage"
msgid "%1%"
msgstr "%1%"

#: contents/ui/ListItemBase.qml:303
#, kde-format
msgctxt "only used for sizing, should be widest possible string"
msgid "100%"
msgstr "100%"

#: contents/ui/main.qml:32
#, kde-format
msgid "Audio Volume"
msgstr "音效音量"

#: contents/ui/main.qml:56
#, kde-format
msgid "Audio Muted"
msgstr "音效靜音"

#: contents/ui/main.qml:58
#, kde-format
msgid "Volume at %1%"
msgstr "音量在 %1%"

#: contents/ui/main.qml:75
#, kde-format
msgid "Middle-click to unmute"
msgstr ""

#: contents/ui/main.qml:76
#, kde-format
msgid "Middle-click to mute all audio"
msgstr ""

#: contents/ui/main.qml:237
#, kde-format
msgid "No output device"
msgstr "沒有輸出裝置"

#: contents/ui/main.qml:369
#, kde-format
msgid "Increase Volume"
msgstr "遞增音量"

#: contents/ui/main.qml:375
#, kde-format
msgid "Decrease Volume"
msgstr "遞減音量"

#: contents/ui/main.qml:381
#, kde-format
msgid "Mute"
msgstr "靜音"

#: contents/ui/main.qml:387
#, kde-format
msgid "Increase Microphone Volume"
msgstr "增加麥克風音量"

#: contents/ui/main.qml:393
#, kde-format
msgid "Decrease Microphone Volume"
msgstr "遞減麥克風音量"

#: contents/ui/main.qml:399
#, kde-format
msgid "Mute Microphone"
msgstr "麥克風靜音"

#: contents/ui/main.qml:503
#, kde-format
msgid "Devices"
msgstr "裝置"

#: contents/ui/main.qml:510
#, kde-format
msgid "Applications"
msgstr "應用程式"

#: contents/ui/main.qml:531 contents/ui/main.qml:533 contents/ui/main.qml:759
#, kde-format
msgid "Force mute all playback devices"
msgstr "強制靜音所有播放裝置"

#: contents/ui/main.qml:567
#, kde-format
msgid "No output or input devices found"
msgstr "找不到輸出或輸入裝置"

#: contents/ui/main.qml:586
#, kde-format
msgid "No applications playing or recording audio"
msgstr "沒有正在播放或錄音的應用程式"

#: contents/ui/main.qml:737
#, kde-format
msgid "Raise maximum volume"
msgstr "提高最大音量"

#: contents/ui/main.qml:763
#, fuzzy, kde-format
#| msgid "No output device"
msgid "Show virtual devices"
msgstr "沒有輸出裝置"

#: contents/ui/main.qml:769
#, kde-format
msgid "&Configure Audio Devices…"
msgstr ""

#: contents/ui/StreamListItem.qml:24
#, kde-format
msgid "Stream name not found"
msgstr "找不到串流名稱"

#~ msgid "General"
#~ msgstr "一般"

#~ msgid "Volume step:"
#~ msgstr "音量步進："

#~ msgid "Audio volume"
#~ msgstr "音訊音量"

#~ msgid "Microphone sensitivity"
#~ msgstr "麥克風靈敏度"

#~ msgid "Mute state"
#~ msgstr "靜音狀態"

#~ msgid "Default output device"
#~ msgstr "預設輸出裝置"

#~ msgid "Record all audio via this device"
#~ msgstr "透過此裝置錄製所有音訊"

#~ msgid "Play all audio via this device"
#~ msgstr "透過此裝置播放所有音訊"

#~ msgctxt ""
#~ "Heading for a list of ports of a device (for example built-in laptop "
#~ "speakers or a plug for headphones)"
#~ msgid "Ports"
#~ msgstr "連接埠"

#~ msgctxt "Port is unavailable"
#~ msgid "%1 (unavailable)"
#~ msgstr "%1（無法使用）"

#~ msgctxt "Port is unplugged"
#~ msgid "%1 (unplugged)"
#~ msgstr "%1（未插入）"

#~ msgctxt ""
#~ "Heading for a list of possible output devices (speakers, headphones, ...) "
#~ "to choose"
#~ msgid "Play audio using"
#~ msgstr "播放音樂透過"

#~ msgctxt ""
#~ "Heading for a list of possible input devices (built-in microphone, "
#~ "headset, ...) to choose"
#~ msgid "Record audio using"
#~ msgstr "錄製聲音透過"

#~ msgid "Feedback:"
#~ msgstr "回饋："

#~ msgid "Play sound when volume changes"
#~ msgstr "音量變更時播放音效"

#~ msgid "Display notification when default output device changes"
#~ msgstr "預設輸出裝置變更時顯示通知"

#~ msgid "Maximum volume:"
#~ msgstr "最大音量："

#~ msgid "Default Device"
#~ msgstr "預設裝置"

#~ msgid "Playback Streams"
#~ msgstr "播放串流"

#~ msgid "Recording Streams"
#~ msgstr "錄製串流"

#~ msgid "Playback Devices"
#~ msgstr "播放裝置"

#~ msgid "Recording Devices"
#~ msgstr "錄製裝置"

#~ msgctxt "label of stream items"
#~ msgid "%1 (%2)"
#~ msgstr "%1 (%2)"
